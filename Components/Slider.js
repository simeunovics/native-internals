import React from 'react';
import { StyleSheet, View, Text, Slider as NativeSlider } from 'react-native';
import { gray } from '../utils/colors';

export default function Slider({ max, unit, step, value, onChange }) {
  return (
    <View style={styles.container}>
      <NativeSlider
        style={{flex: 1}}
        step={step}
        value={value}
        maximumValue={max}
        minimumValue={0}
        onValueChange={onChange}
      />
      <View style={styles.metricCounter}>
        <Text style={{ fontSize: 24 }}>{value}</Text>
        <Text style={{ fontSize: 18, color: gray }}>{unit}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  metricCounter: {
    width: 85,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
