import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { white } from '../utils/colors';
import MetricCard from './MetricCard';
import { addEntry } from '../actions';
import { removeEntry } from '../utils/api';
import { timeToString, getDailyRemainderValue } from '../utils/helpers';
import TextButton from './TextButton';

class EntryDetail extends Component {
  static navigationOptions = ({ navigation }) => {
    const { entryId } = navigation.state.params;
    const [day, month, year] = [
      entryId.slice(0, 4),
      entryId.slice(5, 7),
      entryId.slice(8),
    ];
    const title = `${day} / ${month} / ${year}`;

    return { title };
  };

  reset = () => {
    const { remove, goBack, entryId } = this.props;

    remove();
    goBack();
    removeEntry(entryId);
  };
  shouldComponentUpdate(nextProp) {
    return nextProp.metrics !== null && !nextProp.metrics.today;
  }
  render() {
    const { metrics, entryId } = this.props;
    return (
      <View style={styles.container}>
        <MetricCard metrics={metrics} />
        <TextButton onPress={this.reset} style={{ margin: 20 }}>
          Remove
        </TextButton>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: white,
    padding: 15,
  },
});

const mapStateToProps = (state, { navigation }) => {
  const { entryId } = navigation.state.params;

  return {
    entryId,
    metrics: state[entryId],
  };
};

const mapDispatchToProps = (dispatch, { navigation }) => {
  const { entryId } = navigation.state.params;

  return {
    remove: () =>
      dispatch(
        addEntry({
          [entryId]:
            timeToString() === entryId ? getDailyRemainderValue() : null,
        }),
      ),
    goBack: () => navigation.goBack(),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(EntryDetail);
