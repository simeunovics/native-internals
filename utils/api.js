import { AsyncStorage } from 'react-native';
import { CALENDAR_STORAGE_KEY, formatCalendarResults } from './_calendar';

export const fetchCalendarResults = () => {
  return AsyncStorage.getItem(CALENDAR_STORAGE_KEY).then(formatCalendarResults);
};

export const submitEntry = ({ entry, key }) => {
  return AsyncStorage.mergeItem(
    CALENDAR_STORAGE_KEY,
    JSON.stringify({
      [key]: entry,
    }),
  );
};

export const removeEntry = ({ key }) => {
  return AsyncStorage.getItem(CALENDAR_STORAGE_KEY)
    .then(results => {
      const entries = JSON.parse(results);

      entries[key] = undefined;
      delete entries[key];

      return entries;
    })
    .then(entries =>
      AsyncStorage.setItem(CALENDAR_STORAGE_KEY, JSON.stringify(entries)),
    );
};
